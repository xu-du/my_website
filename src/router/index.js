import { createRouter, createWebHistory } from 'vue-router'

import { get_token, remove_token } from "@/utils/token";
const token = get_token()
const router = createRouter({
    history: createWebHistory(import.meta.env.BASE_URL),
    routes: [
        {
            path: '/',
            component: () => import('../views/Home/index.vue'),
            name: 'home',
            label: '首页',
            meta: {
                isShow: true
            }
        },

        {
            path: '/about',
            component: () => import('../views/About/index.vue'),
            name: 'about',
            label: '关于',
            meta: {
                isShow: true
            }
        },

        {
            path: '/my',
            component: () => import('../views/My/index.vue'),
            name: 'my',
            label: '我的',
            meta: {
                isShow: true
            }
        }
        ,

        {
            path: '/login',
            component: () => import('../views/Login/index.vue'),
            name: 'login',
            label: '登录',
            meta: {
                isShow: true
            },
            // beforeEnter: (to, from, next) => {
            //     if(to.path == '/login' && token) {
            //         next({name:'home'})
            //     }else {
            //         next()
            //         // next('/login')
            //     }
            // }

        },
        {
            path: '/Register',
            component: () => import('../views/Register/index.vue'),
            name: 'register',
            label: '注册',
            meta: {
                isShow: false
            }
        },
        {
            path: '/404',
            component: () => import('../views/404/index.vue'),
            name: '404',
            label: '404',
            meta: {
                isShow: false
            }
        }

    ]
})


// 在路由跳转之前,先判断当前页面是否存在,如果存在放行,如果不存在则跳转 404页面
// to.matched  是 Vue Router 中的一个属性，它表示当前路由是否匹配。如果当前路由匹配，则  to.matched  的值为  true ，否则为  false 。

router.beforeEach((to, from, next) => {
    // 判断token是否过期
    const now = new Date(); // 获取当前时间
    const minutes = 30; // 设置要增加的分钟数
    const timeStamp = now.getTime() + minutes * 60 * 1000; // 计算30分钟后的时间戳
    let date = new Date().getTime()

    const TimeStart = localStorage.getItem('TokenStartTime')

    // 判断 现在时间的时间戳 - 保存token时的时间戳 是否大于0  
    // 如果大于0 则证明token过期 否则未过期
    if ((date - TimeStart) - timeStamp > 0) {
        remove_token()
    }
    // 判断路由是否为空
    if (!to.matched.length) {
        // 跳转到404页面
        next({ path: '404' })
    } else {
        // 继续执行
        next()

    }

})
export default router
