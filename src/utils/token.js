// 定义设置和获取本地存储 token 的方法
// 导出一个函数，用于设置token
export const set_token = (token) => {
    // 将token存储到本地
    localStorage.setItem('token', token)
}

// 导出一个函数，用于获取token
export const get_token = () => {
    // 从本地获取token
    return localStorage.getItem('token')

}

// 导出一个函数，用于移除token
export const remove_token = () => {
    // 从本地移除token
    localStorage.removeItem('token')
    localStorage.removeItem('TokenStartTime')
}

export const set_userInfo = ({ username, avatar }) => {
    localStorage.setItem('userInfo', JSON.stringify({ username, avatar }))
}

export const get_userInfo = () => { 
    return localStorage.getItem('userInfo')
}
export const remove_userInfo = () => {
    localStorage.removeItem('userInfo')
    localStorage.removeItem('user')
}
