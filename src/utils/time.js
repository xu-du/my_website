// 登录成功时,提示上午好 or 中午好 or 下午好

export const getTime = ()=>{
    const hours = new Date().getHours();
    
    if (hours < 12) {
        return '上午好';
    } else if (hours < 13) {
        return '中午好';
    } else if (hours < 18) {
        return '下午好';
    }else {
        return '晚上好';
    }
}