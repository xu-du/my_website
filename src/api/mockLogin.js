import Mock from 'mockjs'
function createUser() {
    return [
        {
            userId: 1,
            avatar:
                'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',
            username: 'admin',
            password: '111111',
            desc: '平台管理员',
            roles: ['平台管理员'],
            buttons: ['cuser.detail'],
            routes: ['home'],
        }
    ]
}
Mock.mock('/api/user/login', 'post', ({ body }) => {
    // 将body转换为JSON格式
    let result = JSON.parse(body)
    // 打印result
    let data
    // 创建用户
    const res = createUser()
    // 查找用户是否存在
    let user = res.find(item => item.username === result.username)
    if (user) {
        if (result.username === res[0].username && result.password === res[0].password) {
            // 正确时，返回token、用户名、头像
            data = Mock.mock({
                "token": '@string(20,25)',
                "username": result.username,
                "avatar": 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif',

            })
            return {
                status: 200,
                message: '登录成功',
                
                data

            }
        } else {
            return {
                status: 400,
                message: '用户名或密码错误',
                data: null
            }
        }

    }


})