import http from "@/utils/http";
export const reqUserInfo = () => {
    return http({
        url:'/admin/acl/index/info',
    })
}
export const reqLogout = () => {
    return http({
        url:'/admin/acl/index/logout',
        method:'post'
    })
}