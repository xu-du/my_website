import http from "@/utils/http";
export const registerAPI = ({username,password})=>{
    return http({
        url:'/register',
        data:{
            username,
            password
        }
    })
}