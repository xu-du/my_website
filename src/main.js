import '@/style.css'
import '@/assets/styles/index.css'
import { createApp } from 'vue'
import { createPinia } from 'pinia'
import "element-plus/theme-chalk/el-notification.css";
import App from './App.vue'
import router from './router'
import '@/api/mockLogin'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
const app = createApp(App)
const pinia = createPinia()
pinia.use(piniaPluginPersistedstate)
import * as ElementPlusIconsVue from '@element-plus/icons-vue' 
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
  app.component(key, component)
}
app.use(pinia)
app.use(router)

app.mount('#app')
