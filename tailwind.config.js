/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      backgroundImage:{
        'base-img':'url("https://z1.ax1x.com/2023/10/28/pieBK4f.jpg")'
      }
    },
  },
  plugins: [],
}

